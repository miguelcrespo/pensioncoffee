'use strict'

describe 'Controller: GeographicCtrl', ->

  # load the controller's module
  beforeEach module 'pensionApp'

  GeographicCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    GeographicCtrl = $controller 'GeographicCtrl', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(scope.awesomeThings.length).toBe 3
