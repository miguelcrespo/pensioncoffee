'use strict'

describe 'Controller: NewpropertyCtrl', ->

  # load the controller's module
  beforeEach module 'pensionApp'

  NewpropertyCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    NewpropertyCtrl = $controller 'NewpropertyCtrl', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(scope.awesomeThings.length).toBe 3
