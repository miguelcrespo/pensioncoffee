'use strict'

describe 'Controller: OwnerpropertyCtrl', ->

  # load the controller's module
  beforeEach module 'pensionApp'

  OwnerpropertyCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    OwnerpropertyCtrl = $controller 'OwnerpropertyCtrl', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(scope.awesomeThings.length).toBe 3
