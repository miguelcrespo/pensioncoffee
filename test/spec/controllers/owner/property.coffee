'use strict'

describe 'Controller: OwnerPropertyCtrl', ->

  # load the controller's module
  beforeEach module 'pensionApp'

  OwnerPropertyCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    OwnerPropertyCtrl = $controller 'OwnerPropertyCtrl', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(scope.awesomeThings.length).toBe 3
