'use strict'

describe 'Controller: MyrequestsCtrl', ->

  # load the controller's module
  beforeEach module 'pensionApp'

  MyrequestsCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    MyrequestsCtrl = $controller 'MyrequestsCtrl', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(scope.awesomeThings.length).toBe 3
