'use strict'

describe 'Controller: OwnerCtrl', ->

  # load the controller's module
  beforeEach module 'pensionApp'

  OwnerCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    OwnerCtrl = $controller 'OwnerCtrl', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(scope.awesomeThings.length).toBe 3
