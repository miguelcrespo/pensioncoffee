'use strict'

describe 'Controller: PropertyCtrl', ->

  # load the controller's module
  beforeEach module 'pensionApp'

  PropertyCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    PropertyCtrl = $controller 'PropertyCtrl', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(scope.awesomeThings.length).toBe 3
