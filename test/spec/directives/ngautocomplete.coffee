'use strict'

describe 'Directive: ngAutocomplete', ->

  # load the directive's module
  beforeEach module 'pensionApp'

  scope = {}

  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()

  it 'should make hidden element visible', inject ($compile) ->
    element = angular.element '<ng-autocomplete></ng-autocomplete>'
    element = $compile(element) scope
    expect(element.text()).toBe 'this is the ngAutocomplete directive'
