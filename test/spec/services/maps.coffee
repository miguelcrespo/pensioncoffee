'use strict'

describe 'Service: maps', ->

  # load the service's module
  beforeEach module 'pensionApp'

  # instantiate service
  maps = {}
  beforeEach inject (_maps_) ->
    maps = _maps_

  it 'should do something', ->
    expect(!!maps).toBe true
