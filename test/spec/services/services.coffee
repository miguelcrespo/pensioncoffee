'use strict'

describe 'Service: services', ->

  # load the service's module
  beforeEach module 'pensionApp'

  # instantiate service
  services = {}
  beforeEach inject (_services_) ->
    services = _services_

  it 'should do something', ->
    expect(!!services).toBe true
