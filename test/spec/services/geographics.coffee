'use strict'

describe 'Service: geographics', ->

  # load the service's module
  beforeEach module 'pensionApp'

  # instantiate service
  geographics = {}
  beforeEach inject (_geographics_) ->
    geographics = _geographics_

  it 'should do something', ->
    expect(!!geographics).toBe true
