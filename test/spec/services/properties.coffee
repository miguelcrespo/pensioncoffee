'use strict'

describe 'Service: properties', ->

  # load the service's module
  beforeEach module 'pensionApp'

  # instantiate service
  properties = {}
  beforeEach inject (_properties_) ->
    properties = _properties_

  it 'should do something', ->
    expect(!!properties).toBe true
