'use strict'

###*
 # @ngdoc function
 # @name pensionApp.controller:TabCtrl
 # @description
 # # TabCtrl
 # Controller of the pensionApp
###
angular.module('pensionApp')
  .controller 'TabCtrl', ($scope) ->
    $scope.activeTab = 1

    $scope.isSelected = (id) ->
      console.log "Is selected tab #{id} "+$scope.activeTab is id
      return $scope.activeTab is id

    $scope.selectTab = (id) ->
      $scope.activeTab = id
