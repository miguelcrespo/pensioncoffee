'use strict'

###*
 # @ngdoc function
 # @name pensionApp.controller:LoginCtrl
 # @description
 # # LoginCtrl
 # Controller of the pensionApp
###
angular.module('pensionApp')
  .controller 'LoginCtrl', ($scope, loginService, sessionService, $location, $auth) ->
    if not $scope.user? and sessionService.get('id')?
      $scope.user = {}
      $scope.user.name = sessionService.get('name')
      $scope.user.id = sessionService.get('id')

    $scope.login = (user) ->
      $auth.login(
        {
          email: user.email
          password: user.password
        }
      ).then( (response) ->
        $scope.user = {}
        data = response.data.data
        console.log "data"
        console.log data
        $scope.user.name = data.name
        $scope.user.id = data.id

        sessionService.set('id', data.id)
        sessionService.set('name', data.name)
        sessionService.set('email', data.email)
        $location.path('/module')
      )

    $scope.logout = ->
      $auth.logout()

    $scope.authenticate = (provider) ->
      $auth.authenticate(provider).then (response) ->
        console.log "Response: "
        console.log response.data
        $scope.user = {}
        data = response.data.data
        $scope.user.name = data.name
        $scope.user.id = data.id

        sessionService.set('id', data.id)
        sessionService.set('name', data.name)
        sessionService.set('email', data.email)
        $location.path('/module')

    $scope.isAuthenticated = ->
      return $auth.isAuthenticated()
