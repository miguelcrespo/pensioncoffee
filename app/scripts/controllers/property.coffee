'use strict'

###*
 # @ngdoc function
 # @name pensionApp.controller:PropertyCtrl
 # @description
 # # PropertyCtrl
 # Controller of the pensionApp
###
angular.module('pensionApp')
.controller 'PropertyCtrl', ($scope, $stateParams, propertiesServices, mapsService) ->
  propertyid = $stateParams.id
  Map = mapsService.getMap()
  $scope.map = new Map(11.236192, -74.194066, 12)

  promise = propertiesServices.propertyById(propertyid)
  promise.then (response) ->
    $scope.property = response
    $scope.map.center.latitude = $scope.property.latitude
    $scope.map.center.longitude = $scope.property.longitude
    $scope.map.zoom = 15
    $scope.map.addMarker($scope.property.latitude, $scope.property.longitude)

  $scope.mySlides = [
    "http://placehold.it/500x200",
    "http://placehold.it/500x210"
  ]

  $scope.applyRoom = (id) ->
    promise = propertiesServices.applyForRoom(id)
    promise.then (response) ->
      if response.error
        alert response.description
      else
        for room in $scope.property.rooms
          if room.id is id
            room.applied = true


  ###
  promise = propertiesServices.propertyById(propertyid)
  $scope.owner = false
  promise.then (response) ->
    if not response.error
      $scope.property = response.data
      if parseInt(sessionService.get('id')) is $scope.property.owner.id
        $scope.owner = true
    else
      console.error "Error was: #{respoanse.description}"
      for i in response
        alert "asdsa"

  $scope.applyForBed = (bed) ->
    promise = propertiesServices.applyForBed(bed)
    promise.then (response) ->
      if not response.error
        alert response.description
        $scope.property.yourrequest = {
          approved : 0
        }
      else
        alert response.description
###
