'use strict'

###*
 # @ngdoc function
 # @name pensionApp.controller:GeographicCtrl
 # @description
 # # GeographicCtrl
 # Controller of the pensionApp
###
angular.module('pensionApp')
  .controller 'GeographicCtrl', ($scope, geographicsService, propertiesServices, mapsService) ->
    # Vars


    promise = geographicsService.get()
    promise.then (response) ->
      if not response.data.error
        $scope.geographic = response.data.data

    Map = mapsService.getMap()
    $scope.map = new Map(65.240226, -74.202794, 12)

    $scope.chargeMap = ->
      $scope.map.center.latitude = $scope.cityselected.latitude
      $scope.map.center.longitude = $scope.cityselected.longitude
      $scope.map.zoom = parseInt($scope.cityselected.zoom, 10)

    # Check if place has changed
    $scope.$watch 'placedetails', ->
      if $scope.placedetails?
        $scope.map.addCircle($scope.placedetails.geometry.location.k,
        $scope.placedetails.geometry.location.B, 2000,
        {color: '#5D994E', weight: 2, opacity: 1},
        {color: '#DCE8DF', opacity: 0.2})
        #Deferer for charge properties in selected city
        propertiesServices.propertiesByCity($scope.cityselected.id).then (response) ->
          #console.info "Properties:"
          #console.info response
          if not response.error
            $scope.properties = mapsService.distanceHaversine $scope.map,response.data
            console.log "Properties: "
            console.log $scope.properties
            for property in $scope.properties
              $scope.map.addMarker(property.latitude, property.longitude)
      else
        console.log "placedetails is null"
