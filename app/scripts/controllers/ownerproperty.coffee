'use strict'

###*
 # @ngdoc function
 # @name pensionApp.controller:OwnerpropertyCtrl
 # @description
 # # OwnerpropertyCtrl
 # Controller of the pensionApp
###
angular.module('pensionApp')
.controller 'OwnerpropertyCtrl', ($scope, $routeParams, propertiesServices, sessionService) ->
  propertyid = $routeParams.id
  promise = propertiesServices.propertyById(propertyid)
  $scope.owner = false
  promise.then (response) ->
    console.log response
    if not response.error
      $scope.property = response.data
      if parseInt(sessionService.get('id')) is $scope.property.owner.id
        $scope.owner = true
    else
      console.error "Error was: #{response.description}"
      for i in response
        alert "asdsa"
