'use strict'

###*
 # @ngdoc function
 # @name pensionApp.controller:MainCtrl
 # @description
 # # MainCtrl
 # Controller of the pensionApp
###
angular.module('pensionApp')
  .controller 'MainCtrl', ($scope, sessionService) ->
  ###
    $scope.user = {}
    if sessionService.get('id')?
      $scope.user.id = sessionService.get('id')
      $scope.user.name = sessionService.get('name')
      $scope.user.email = sessionService.get('email')
    else
      $scope.user.id = null

    console.log $scope.user.id
###