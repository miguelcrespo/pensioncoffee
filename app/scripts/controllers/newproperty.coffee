'use strict'

###*
 # @ngdoc function
 # @name pensionApp.controller:NewpropertyCtrl
 # @description
 # # NewpropertyCtrl
 # Controller of the pensionApp
###
angular.module('pensionApp').controller 'NewpropertyCtrl', ($scope, mapsService, geographicsService, $upload, servicesService, propertiesServices) ->
  Map = mapsService.getMap()
  $scope.map = new Map(11.236192, -74.194066, 12)
  console.log "Mapa"
  console.log $scope.map
  $scope.property = {}
  $scope.property.is_available = true
  $scope.property.rooms = []
  promise = servicesService.get()
  promise.then (response) ->
    $scope.services = response

  promise = geographicsService.get()
  promise.then (response) ->
    console.log response
    if not response.error
      $scope.geographic = response.data

  $scope.findLatLng = () ->
    if $scope.property?.address? and $scope.cityselected?
      promise = mapsService.geoCoding $scope.property.address, $scope.cityselected, $scope.countryselected?
      promise.then (response) ->
        $scope.map.center.latitude = response[0].geometry.location.k
        $scope.map.center.longitude = response[0].geometry.location.D
        $scope.map.zoom = 16
        if $scope.map.markers.length is 0
          $scope.map.addMarker(response[0].geometry.location.k, response[0].geometry.location.D)


        $scope.property.latitude = $scope.map.markers[0].latitude
        $scope.property.longitude = $scope.map.markers[0].longitude
      , (error) ->
        console.log error

  $scope.property.paths = []

  $scope.onFileSelect = ($files) ->
  #$files: an array of files selected, each file has name, size, and type.
  #for (var i = 0; i < $files.length; i++) {
    $scope.property.paths = []
    for file in $files
    #var $file = $files[i];
      $upload.upload({
        url: 'http://localhost/pension/public/upload/property-images',
        file: file,
      }).then (data, status, headers, config) ->
        # file is uploaded successfully
        console.log file
        $scope.property.paths.push data.data.data.filename

    console.log $scope.property.paths

  $scope.addRoom = () ->

    $scope.property.rooms.push
      price: 0
      services: JSON.parse(JSON.stringify($scope.services))
      is_available : true


  $scope.addBed = (room) ->
    if not room.beds?
      room.beds = []

    room.beds.push
      price: 0
      is_available : true

  $scope.removeRoom = (rooms, index) ->
    rooms.splice(index, 1)

  $scope.removeBed = (beds, index) ->
    beds.splice(index, 1)

  $scope.createProperty = () ->
    for room in $scope.property.rooms
      services = []
      services.push(service) for service in room.services when service.ticked? and service.ticked
      room.services2 = services
    propertiesServices.createProperty($scope.property)
    console.log $scope.property
