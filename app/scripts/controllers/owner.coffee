'use strict'

###*
 # @ngdoc function
 # @name pensionApp.controller:OwnerCtrl
 # @description
 # # OwnerCtrl
 # Controller of the pensionApp
###
angular.module('pensionApp')
.controller 'OwnerCtrl', ($scope, mapsService, geographicsService, propertiesServices, sessionService, servicesService) ->
  ###
  $scope.modernWebBrowsers = [
    { name: "Opera",              maker: "(Opera Software)",        ticked: true  , id: 1},
  { name: "Internet Explorer",  maker: "(Microsoft)",             ticked: false , id:2},
  {name: "Firefox",            maker: "(Mozilla Foundation)",    ticked: true  , id:3},
  {name: "Safari",             maker: "(Apple)",                 ticked: false , id:4},
  {name: "Chrome",             maker: "(Google)",                ticked: true  , id:5}
  ]
  ###
  Map = mapsService.getMap()
  $scope.map = new Map(65.240226, -74.202794, 12)
  $scope.showaddproperty = false
  #Properties of the user
  promise = propertiesServices.myProperties()

  promise.then (response) ->
    console.info response
    $scope.properties = response.data

  $scope.property = {}
  $scope.property.rooms = []
  # services to add
  promise = servicesService.get()

  promise.then (response) ->
    console.log "Services: "
    console.info response.data
    $scope.services = response.data

  promise = geographicsService.get()
  promise.then (response) ->
    if not response.data.error
      $scope.geographic = response.data.data

  $scope.findLatLng = () ->
    if $scope.property?.address? and $scope.cityselected?
      promise = mapsService.geoCoding $scope.property.address, $scope.cityselected, $scope.countryselected?
      promise.then (response) ->
        $scope.map.center.latitude = response[0].geometry.location.k
        $scope.map.center.longitude = response[0].geometry.location.B
        $scope.map.zoom = 16

        $scope.map.addMarker(response[0].geometry.location.k, response[0].geometry.location.B)

        $scope.property.latitude = $scope.map.markers[0].latitude
        $scope.property.latitude = $scope.map.markers[0].longitude
      , (error) ->
        console.log error

  $scope.createProperty = (property) ->
    property.city = $scope.cityselected.id
    $scope.property.latitude = $scope.map.markers[0].latitude
    $scope.property.longitude = $scope.map.markers[0].longitude

    promise = propertiesServices.createProperty($scope.property)

    promise.then (response) ->
      if not response.error
        alert "Property added"
      else
        alert response.description

  $scope.getNumber = (num) ->
    return new Array(num)

  $scope.addRoom = () ->
    services = []
    for service in $scope.services
      services.push({id: service.id, name: service.name, icon: service.icon, ticked: false})

    $scope.property.rooms.push({price: 0, services: services})

  $scope.toggleAddProperty = () ->
    if $scope.showaddproperty
      $scope.showaddproperty = false
    else
      $scope.showaddproperty = true
