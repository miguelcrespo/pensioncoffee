'use strict'

###*
 # @ngdoc function
 # @name pensionApp.controller:MyrequestsCtrl
 # @description
 # # MyrequestsCtrl
 # Controller of the pensionApp
###
angular.module('pensionApp')
  .controller 'MyrequestsCtrl', ($scope) ->
    $scope.awesomeThings = [
      'HTML5 Boilerplate'
      'AngularJS'
      'Karma'
    ]
