'use strict'

###*
 # @ngdoc function
 # @name pensionApp.controller:ProfileCtrl
 # @description
 # # ProfileCtrl
 # Controller of the pensionApp
###
angular.module('pensionApp')
  .controller 'ProfileCtrl', ($scope, userService, propertiesServices) ->
    promise = userService.me()

    promise.then (response) ->
      $scope.user = response

    $scope.activeTab = 1

    $scope.isSelected = (id) ->
      console.log "Is selected tab #{id} "+$scope.activeTab is id
      return $scope.activeTab is id

    $scope.selectTab = (id) ->
      $scope.activeTab = id

    $scope.acceptRequest = (request) ->

      promise = propertiesServices.actionRequest(request.id, 1, true)

      promise.then (response) ->
        console.info response

    $scope.ignoreRequest = (request) ->
      console.log "Request"
      console.info request
