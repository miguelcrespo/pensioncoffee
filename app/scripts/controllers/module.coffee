'use strict'

###*
 # @ngdoc function
 # @name pensionApp.controller:ModuleCtrl
 # @description
 # # ModuleCtrl
 # Controller of the pensionApp
###
angular.module('pensionApp')
  .controller 'ModuleCtrl', ($scope, servicesService,mapsService,propertiesServices) ->


    #parameters to search properties
    $scope.search = {}
    $scope.search.advanced = false

    $scope.search.price =
    min: 50000
    max: 500000

    $scope.search.distance =
    max: 10000

    Map = mapsService.getMap()
    $scope.map = new Map(11.236192, -74.194066, 15)

    promise = servicesService.get()
    promise.then (response) ->
      $scope.search.services = response

    $scope.$watch 'search.placedetails', ->
      if $scope.search.placedetails?
        console.log $scope.search
        $scope.map.addCircle($scope.search.placedetails.geometry.location.k,
        $scope.search.placedetails.geometry.location.D, 2000,
        {color: '#5D994E', weight: 2, opacity: 1},
        {color: '#DCE8DF', opacity: 0.2})

    $scope.searchProperty = ->
      promise = propertiesServices.searchProperty $scope.structureSearch $scope.search

      promise.then (response) ->
        $scope.properties = response
        $scope.map.clearMarkers()
        for propertytemp in $scope.properties
          min = 99999999999
          for room in propertytemp.rooms
            console.log room.price
            if room.price < min
              min = room.price
          propertytemp.min = min
          propertytemp.distance = mapsService.distanceHaversine($scope.map , [propertytemp])
          $scope.map.addMarker propertytemp.latitude, propertytemp.longitude

    $scope.structureSearch = (search2) ->
      services = []
      services.push(service) for service in search2.services when service.ticked? and service.ticked
      console.log services
      city = $scope.findCity(search2.placedetails.address_components)
      search = {
        place: {
          name: search2.place
          city: city
        }
        latitude: search2.placedetails.geometry.location.k
        longitude: search2.placedetails.geometry.location.D
        advanced: search2.advanced
        price: search2.price
        distance: search2.distance
        services: services
        type: search2.type
      }
      console.log search
      console.log search2
      return search

    $scope.findCity = (components) ->
      for component in components
        if component.types[0] is "locality" and component.types[1] is "political"
          return {
            short_name: component.short_name
            long_name: component.long_name
          }
      return null
