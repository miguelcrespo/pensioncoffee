'use strict'

###*
 # @ngdoc directive
 # @name pensionApp.directive:ngAutocomplete
 # @description
 # # ngAutocomplete
###
angular.module('pensionApp')
  .directive('ngAutocomplete', ($parse) ->
    opts = {}
    scope:
      details: '='
      ngAutocomplete: '='
      options: '='
    link: (scope, element, attrs, model) ->
      initOpts = ->
        if scope.options
          if scope.options.types
            opts.types = []
            opts.types.push(scope.options.types)
          if scope.options.bounds
            opts.bounds = scope.options.bounds
          if scope.options.country
            opts.componentRestrictions = {
              country: scope.options.country
            }
      initOpts()
      newAutoComplete = ->
        scope.gPlace = new google.maps.places.Autocomplete element[0], opts
        google.maps.event.addListener scope.gPlace, 'place_changed', ->
          scope.$apply ->
            scope.details = scope.gPlace.getPlace()
            scope.ngAutocomplete = element.val()

      newAutoComplete()
      scope.watchOptions = ->
        scope.options
      scope.$watch scope.watchOptions,( ->
        initOpts()
        newAutoComplete()
        element[0].value = ''
        scope.ngAutocomplete = element.val()
      ), true
    )
