'use strict'

###*
 # @ngdoc overview
 # @name pensionApp
 # @description
 # # pensionApp
 #
 # Main module of the application.
###
angular
.module('pensionApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ui.router',
    'ngRoute'
    'ngSanitize',
    'ngTouch',
    'uiGmapgoogle-maps',
    'multi-select',
    'ui-rangeSlider',
    'angular-flexslider',
    'satellizer',
    'angularFileUpload'
  ])
.config ($urlRouterProvider,$stateProvider,$authProvider) ->
  api = "http://127.0.0.1/pension/public/"
  $authProvider.loginUrl = api+'auth/login'
  $authProvider.signupRedirect = '/'

  $stateProvider
  .state 'home',
    url: '/'
    templateUrl: 'views/main.html'
    controller: 'MainCtrl'

  .state 'owner',
    url: '/owner'
    templateUrl: 'views/owner.html'
    controller: 'OwnerCtrl'

  .state 'module',
    url: '/module'
    templateUrl: 'views/module.html'
    controller: 'ModuleCtrl'
    resolve:
      authenticated: ($location, $auth) ->
        if !$auth.isAuthenticated()
          return $location.path('/')

  .state 'property',
    url: '/property/:id'
    templateUrl: 'views/property.html'
    controller: 'PropertyCtrl'

  .state 'profile',
    url: '/profile'
    templateUrl: 'views/profile.html'
    controller: 'ProfileCtrl'
    resolve:
      authenticated: ($location, $auth) ->
        if !$auth.isAuthenticated()
          return $location.path('/')


  .state 'newproperty',
    url: '/newproperty'
    templateUrl: 'views/newproperty.html'
    controller: 'NewpropertyCtrl'
  $urlRouterProvider.otherwise('/')


  #Auth services
  $authProvider.facebook
    clientId: '532808513467613'

  $authProvider.facebook(
    url: api+'auth/facebook'
  )
