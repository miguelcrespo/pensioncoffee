'use strict'

###*
 # @ngdoc service
 # @name pensionApp.properties
 # @description
 # # properties
 # Factory in the pensionApp.
###
angular.module('pensionApp')
.factory 'propertiesServices', ($http, $q, api, $auth) ->
  # Service logic
  # ...
  api = api.getApi()
  # Public API here
  {

  searchProperty: (search) ->
    defer = $q.defer()
    $http
      url: "#{api}searchproperties"
      method: "POST"
      params:
        search:search
    .success (response) ->
      defer.resolve(response.data)
    return defer.promise

  actionRequest: (id, type, action) ->
    defer = $q.defer()
    $http
      url: "#{api}actionRequest"
      method: "POST"
      params:
        id: id
        type: type
        action: action
    .success (response) ->
      defer.resolve(response.data)
    return defer.promise

  propertyById: (id) ->
    defer = $q.defer()
    $http
      url: "#{api}property"
      method: "POST"
      params:
        id: id
    .success (response) ->
      defer.resolve(response.data)
    return defer.promise

  applyForRoom: (id) ->
    defer = $q.defer()
    $http
      url: "#{api}roomreserve"
      method: "POST"
      params:
        id: id
    .success (response) ->
      defer.resolve(response)
    return defer.promise

  applyForBed: (bed) ->
    defer = $q.defer()
    $http
      url: "#{api}bedreserve?callback=JSON_CALLBACK"
      method: "JSONP"
      params:
        bed: bed.id
    .success (response) ->
      defer.resolve(response)
    return defer.promise

  createProperty: (property) ->

    defer = $q.defer()
    $http
      url: "#{api}actionRequest"
      method: "POST"
      data: property
    .success (response) ->
      defer.resolve(response)
    return defer.promise
  }
