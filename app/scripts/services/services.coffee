'use strict'

###*
 # @ngdoc service
 # @name pensionApp.api
 # @description
 # # api
 # Factory in the pensionApp.
###
angular.module('pensionApp')
.factory 'servicesService', (api, $http, $q, $auth, sessionService) ->
  # Service logic
  # ...

  #api = "http://127.0.0.1/pension/public/"
  api = api.getApi()

  # Public API here
  {
  get: ->

    console.log "TOKEN:"
    console.log "#{sessionService.get('satellizer_token')}"
    defer = $q.defer()
    $http
      url: "#{api}getservices"
      method: "POST"
    .success (response) ->
      defer.resolve(response.data)
    return defer.promise
  }
