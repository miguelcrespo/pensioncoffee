'use strict'

###*
 # @ngdoc service
 # @name pensionApp.api
 # @description
 # # api
 # Factory in the pensionApp.
###
angular.module('pensionApp')
  .factory 'api', ->
    # Service logic
    # ...

    api = "http://127.0.0.1/pension/public/"
    #api = "http://apiv1.pruebaspensiones.hol.es/public/"

    # Public API here
    {
      getApi: ->
        api
    }
