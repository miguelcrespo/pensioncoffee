'use strict'

###*
 # @ngdoc service
 # @name pensionApp.maps
 # @description
 # # maps
 # Factory in the pensionApp.
###
angular.module('pensionApp')
  .factory 'mapsService', ($q, $log) ->
    # Service logic
    # ...

    # Public API here
    {
      DistanceBetweenPoints: (origin, destinations, $log) ->
        #defer = $q.defer()
        service = new google.maps.DistanceMatrixService()
        _origin = new google.maps.LatLng(origin.center.latitude, origin.center.longitude)
        _destinations = []
        for destination in destinations
          _destinations.push new google.maps.LatLng(destination.latitude, destination.longitude)
        service.getDistanceMatrix
          origins: [_origin]
          destinations: _destinations
          travelMode: google.maps.TravelMode.DRIVING
          unitSystem: google.maps.UnitSystem.METRIC
          avoidHighways: false
          avoidTolls: false
          , (response, status) ->
            alert "Call to Callback"

            if status isnt google.maps.DistanceMatrixStatus.OK
              ## Failure
              #defer.reject(status);
            else
              #defer.resolve(response)
            #return defer.promise

      #Haversine formula
      distanceHaversine: (origin, destinations) ->
        deg2rad = (deg) ->
          return deg * (Math.PI / 180)

        for destination in destinations
          R = 6371
          dLat = deg2rad(destination.latitude - origin.circles[0].center.latitude)
          dLon = deg2rad(destination.longitude - origin.circles[0].center.longitude)
          a =
            Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.cos(deg2rad(origin.circles[0].center.latitude)) * Math.cos(deg2rad(destination.latitude)) *
            Math.sin(dLon/2) * Math.sin(dLon/2)
          c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
          d = (R * c) * 1000
          console.info destination.distancefromcenter = d
          destination

      geoCoding: (address, city, country) ->
        geocoder = new google.maps.Geocoder()
        defer = $q.defer()
        geocoder.geocode {'address': address, 'componentRestrictions': {'country' : 'CO', locality: city.name}}
        , (results, status) ->

          if status is google.maps.GeocoderStatus.OK
            defer.resolve results
          else
            defer.reject "Geocode was not success for the following reason: #{status}"
        return defer.promise
      getMap: ->
        return class Map
          constructor: (latitude, longitude, zoom) ->
            @center =
              latitude: latitude
              longitude: longitude
            @zoom = zoom
            @circles = []
            @markers = []
          addCircle: (latitude, longitude, radius, stroke, fill) ->
            @circles = []
            @circles.push {
              id: @uniqueId(8)
              center:
                latitude: latitude
                longitude: longitude
              radius: radius
              stroke: stroke
                ###color: '#5D994E'
                weight: 2
                opacity: 1###
              fill: fill
                ###color: '#DCE8DF'
                opacity: 0.2###
              geodesic: true
            }
          addMarker: (latitude, longitude) ->
            @markers.push
              id : @uniqueId(8)
              latitude : latitude
              longitude : longitude
              options:
                draggable: false
              events:
                dragend: (marker, eventName, args) ->
                  $log.log('marker dragend')
                  lat = marker.getPosition().lat()
                  lon = marker.getPosition().lng()
                  $log.log(lat)
                  $log.log(lon)

            console.log "Markers: "
            console.info @markers

          clearMarkers: () ->
            @markers = []

          uniqueId: (length=8) ->
            id = ""
            id += Math.random().toString(36).substr(2) while id.length < length
            id.substr 0, length
    }
