'use strict'

###*
 # @ngdoc service
 # @name pensionApp.session
 # @description
 # # session
 # Factory in the pensionApp.
###
angular.module('pensionApp')
  .factory 'sessionService', ->
    # Service logic
    # ...

    # Public API here
    {
      get: (key) ->
        sessionStorage.getItem(key)
      set: (key, value) ->
        sessionStorage.setItem(key, value)
      destroy: (key) ->
        sessionStorage.removeItem(key);
    }
