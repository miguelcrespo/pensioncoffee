'use strict'

###*
 # @ngdoc service
 # @name pensionApp.login
 # @description
 # # login
 # Factory in the pensionApp.
###
angular.module('pensionApp')
.factory 'loginService', ($http, api, $q) ->
  # Service logic
  # ...
  api = api.getApi()

  # Public API here
  {
  isLogged: () ->
    defer = $q.defer()
    $http
      url: "#{api}islogged?callback=JSON_CALLBACK"
      method: "JSONP"
    .success (response) ->
      defer.resolve(response)
    return defer.promise

  }
