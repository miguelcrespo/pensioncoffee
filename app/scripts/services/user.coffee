'use strict'

###*
 # @ngdoc service
 # @name pensionApp.user
 # @description
 # # user
 # Factory in the pensionApp.
###
angular.module('pensionApp')
  .factory 'userService', ($q, $http, api) ->
    # Service logic
    # ...
    api = api.getApi()
    # Public API here
    {

    me: () ->
      defer = $q.defer()
      $http
        url: "#{api}me"
        method: "POST"
      .success (response) ->
        defer.resolve(response.data)
      return defer.promise
    }
