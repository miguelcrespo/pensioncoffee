'use strict'

###*
 # @ngdoc service
 # @name pensionApp.geographics
 # @description
 # # geographics
 # Factory in the pensionApp.
###
angular.module('pensionApp')
  .factory 'geographicsService', ($http,api, $q) ->
    # Service logic
    # ...
    api = api.getApi()

    # Public API heref
    {
      get: ->
        defer = $q.defer()
        $http
          url: "#{api}geographicinformation"
          method: "GET"
        .success (response) ->
          defer.resolve(response)
        return defer.promise

    }
